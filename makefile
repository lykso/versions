PREFIX ?= /usr
DESTDIR ?= $(PREFIX)/bin
GITBASE ?= $(shell realpath $(shell git config --get remote.origin.url) | rev \
           | cut -d/ -f2- | rev)

.PHONY: install uninstall lix-os-pkgs

lix-os-pkgs:
	git clone $(GITBASE)/versions-lix-os-pkgs pkg

install: lix-os-pkgs
	ln -sf $(VERSIONSROOT)/versions.sh $(DESTDIR)/versions

uninstall:
	rm $(DESTDIR)/versions
