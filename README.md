# versions

58 SLOC.

`versions` checks a url, applies a couple regular expressions, sorts the results
using `vercmp` and `shsort`, and returns a sorted list of version numbers for
any package it knows about.

written for lix-os maintainers to check for new versions of packages.  depending
on how the package's versions are presented, it may only return a few of the
most recent version numbers for the package, or it may return the entire version
history.

because parsing and sorting versions using POSIX-compliant shell script can be a
_very_ slow operation, it caches the results of previous sorting operations and
only recomputes the sorted version list if it appears the list of versions at
the package url has changed.

all package configuration and cache files are kept in subdirectories of the
`pkg` directory.

each package's `url` file is tab-delimited.  it is formatted as follows:

`<url>	<pre regex>	<post regex>	<version regex>`

- `<url>` is a url which contains a list of versions.

- `<pre regex>` is the pattern up to which the content at `url` will be
	discarded before beginning to look for matches for `version regex`.

- `<post regex>` is the pattern after which the content at `url` will be
    discarded before beginning to look for matches for `version regex`.

- `<version regex>` is a pattern containing a subgroup matching the version
   strings `versions` should return.  there are some default version regexes
   defined in versions.sh.  any of these can be referenced in the package's
   file as a shell variable.


## dependencies

vercmp and shsort.


## usage

`./versions.sh <package> [timeout]`

`<package>` is a package name corresponding to a subdirectory in `pkg`.

`[timeout]` is the number of seconds curl will spend trying to get each url
listed in the package's `urls` list file before moving on to the next line.  if
all urls time out, `versions` fails with an error message.
